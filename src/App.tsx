import { Fragment } from 'react'
import './App.css'

function App() {
  let formData: { firstName: string, lastName: string, email: string, password: string } = {
    firstName: "",
    lastName: "",
    email: "",
    password: ""
  }
  const submit = (e: { preventDefault: () => void }) => {
    const errorMessage = (input: string) => {
      document.getElementById(input)?.classList.add('input-error')
      input !== "email" && document.getElementById(input)?.setAttribute('placeholder', '')
      document.getElementById(input + '-error')?.classList.add('display-block')
    }
    const removeErrorMessage = (input: string) => {
      document.getElementById(input)?.classList.remove('input-error')
      document.getElementById(input + '-error')?.classList.remove('display-block')
    }
    e.preventDefault();
    formData.firstName == "" ?
      errorMessage('first-name') : removeErrorMessage('first-name')
    formData.lastName == "" ?
      errorMessage('last-name') : removeErrorMessage('last-name')
    formData.email.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/) == null ?
      errorMessage('email') : removeErrorMessage('email')
    formData.password == "" ?
      errorMessage('password') : removeErrorMessage('password')
    Object.values(formData).every(i => i !== "") && location.reload()
  }

  return (
    <Fragment>
      <main>
        <section className='main-left'>
          <h1 className='h1'>Learn to code by watching others</h1>
          <p className='desc'>
            See how experienced developers solve problems in real-time. Watching scripted tutorials is great, but understanding how developers think is invaluable.
          </p>
        </section>
        <section className='main-right'>
          <div className='promotion'>
            <p><strong>Try it free 7 days</strong> then $<span className='big'>20</span>/mo. thereafter</p>
          </div>
          <form id='form'>
            <input id='first-name' className='input' type="text" placeholder='First Name'
              onChange={(e) => formData.firstName = e.target.value}
              required />
            <label id='first-name-error' className='error-message'>
              First Name cannot be empty
            </label>
            <input id='last-name' className='input' type="text" placeholder='Last Name'
              onChange={(e) => formData.lastName = e.target.value}
              required />
            <label id='last-name-error' className='error-message'>
              Last Name cannot be empty
            </label>
            <input id='email' className='input' type="email" placeholder='Email Address'
              onChange={(e) => formData.email = e.target.value}
              required />
            <label id='email-error' className='error-message'>
              Looks like this is not an email
            </label>
            <input id='password' className='input' type="password" placeholder='Password'
              onChange={(e) => formData.password = e.target.value}
              required />
            <label id='password-error' className='error-message'>
              Password cannot be empty
            </label>
            <input id='submit' type="submit" value="CLAIM YOUR FREE TRIAL" onClick={submit} />
            <p className='terms'>
              By clicking the button, you are agreeing to our <strong>Terms and Services</strong>
            </p>
          </form>
        </section>
      </main>

      <footer className="attribution">
        Challenge by <a href="https://www.frontendmentor.io?ref=challenge" target="_blank" rel="noreferrer">Frontend Mentor</a>.
        Coded by <a href="https://saad-shaikh-portfolio.netlify.app/" target="_blank" rel="noreferrer">Saad Shaikh</a>.
      </footer>
    </Fragment>
  )
}

export default App