# Title: Intro component with sign up form

Tech stack: React, TypeScript & Vite

Deployed project: https://saad-shaikh-intro-component-with-sign-up-form.netlify.app/

## Main tasks:
- Created a responsive page that is adaptable to both mobile and desktop devices by implementing CSS grid
- Created a sign up form with a bespoke form validation with TypeScript


## Desktop view:
![Desktop view](design/desktop-design.jpg) 

## Mobile view:
![Mobile view](design/mobile-design.jpg) 
